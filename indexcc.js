const http = require('http');
// const debug = require('debug')('adnoc');
// debug('starting');
//require('./consumingEvents');
process.env.TZ = 'Asia/Dubai';
const WebSocket = require('ws');




function heartbeatHandler(req, res) {
  if (req.url === '/ev/heartbeat') {
    res.writeHead(200);
    return res.end('ev Up!');
  }

  if (req.url === '/heartbeat') {
    res.writeHead(200);
    return res.end('ev Up!');
  }



  res.writeHead(501); // HTTP 501 Not Implemented
  return res.end();
}

const config = {
  port : 8085
}


async function start() {
  const app = null; // eslint-disable-line global-require
  const httpServer = http.createServer(heartbeatHandler);


  httpServer.listen(config.port, () => {
    console.log('listening on port %s', config.port);
  });
  const wss = new WebSocket.Server({ server: httpServer , path:"/ev/"});

  wss.on('connection', function connection(ws) {

    ws.on('message', function incoming(message) {
      console.log('received: %s', message);
      ws.send('EV App received %s', message);
    });

    ws.send('EV App socket connected');
  });
}

start().catch((e) => {
  throw e;
});



// const express = require('express')
// const bodyParser = require('body-parser')

// const app = express()
// const port = 8085

// app.use(bodyParser.json())

// app.get('/connect', async (req, res) => {
//     console.log('/connect endpoint is invoked')
//     console.log(req)
//     console.log(JSON.stringify(req))
//     res.sendStatus(200)
// })

// app.post('/message', async (req, res) => {
//     console.log('/joinroom endpoint is invoked')
//     res.sendStatus(200)
// })


// app.post('/disconnect', async (req, res) => {
//   console.log('/joinroom endpoint is invoked')
//   res.sendStatus(200)
// })


// app.listen(port, () => {
//     console.log(`App listening on port ${port}`)
// })